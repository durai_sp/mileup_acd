"""
Dashboard.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 12, 2017

Dashboard class has the functionality to load data to S3
"""

from load_data_s3 import LoadDataS3

class Dashboard(object):

    def __init__(self, build, r_name, level):
	self.build = build
	self.region = r_name
	self.level = level


    def handler(self):
	load = LoadDataS3(self.build, self.region, self.level)
	load.get_data()

