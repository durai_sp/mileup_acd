"""
handler.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 11, 2017

handle functionality is used to initiate prod trip event.
"""

from dashboard import Dashboard

def handler():
    dashboard = Dashboard('prod', 'us-east-1', 'DEBUG')  
    dashboard.handler()

handler()
