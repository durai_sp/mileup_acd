"""
LoadDataS3.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 12, 2017

LoadDataS3 class has the functionality to fetch data from DynamoDB and load it in S3
"""

from platformutils.dynamodb_utils import DynamoDBUtils
from platformutils.firehose_utils import FirehoseUtils
from platformutils.utils.time_util import TimeUtil
from platformutils.utils.file_util import FileUtil
import json, ast
from decimal import Decimal
from platformutils.s3_utils import S3Utils

class LoadDataS3(object):

    def __init__(self, build, region_name, level):
	trip_table = 'ppacn-agero-{:}-acd'.format(build)
	self.db = DynamoDBUtils(trip_table, region_name, level)
	self.firehose = FirehoseUtils('{:}-acd'.format(build), region_name, level)
	self.time =TimeUtil()    
	self.file_util =FileUtil()
	self.s3 = S3Utils(region_name, level)
	self.current_directory = self.file_util.get_current_directory()
	self.bucket = 'policypalacnprod'
	

    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)
    	raise TypeError

	
    def get_data(self):
	"""
	This method is used to fetch data from DynamoDB.
	Upload the file to S3
        """
	path = self.current_directory + "/acd-event-metadata-" + str(self.time.get_date(1)) + ".csv"
	utc_value = self.time.get_utc_timestamp(1, 'ms')
	response = self.db.query_table('insertDate', utc_value, 'insertDate-index')
	batch_size = 0
	records = []
	while True:
	    for rows in response['Items']:
		row = json.dumps(rows, default=self.default)
		handled_d = self.handle_filter_data(row)
		records.append(handled_d)
		batch_size = batch_size + 1
		if batch_size == 500:
		    self.put_record(records)
		    records = []
		    batch_size = 0	
#		self.file_util.write_data(path, handled_d)
	    if response.get('LastEvaluatedKey'):
	        response = self.db.query_table('insertDate', utc_value, 'insertDate-index', last_evaluated_key = response['LastEvaluatedKey'])
	    else:
		break
	if batch_size > 0:
	    self.put_record(records)
#	self.file_util.compress_text_file(path)	
#	self.upload_data(path + ".gz")
#	self.file_util.change_directory(self.current_directory)
#	self.file_util.remove_file_start_with_name('acd-event-metadata-')

    def handle_filter_data(self, datas):
	data = json.loads(datas)
	os = data.get('os')
    	deviceModel = data.get('deviceModel')
	if os=="ios":
	    if "," in deviceModel:
    	    	a, b = deviceModel.split(",")
	    	deviceModel_ouput_str = a + "#" + b
	    else:
	        deviceModel_ouput_str = deviceModel
	else:
	    deviceModel_ouput_str = deviceModel

	tripId = data.get('tripId')
	frameUTCTime = int(data.get('frameUTCTime'))
	codeVersion = data.get('codeVersion')
	crashProb = float(data.get('crashProb'))
	crashSeverity = float(data.get('crashSeverity'))
	frameDuration = float(data.get('frameDuration'))
	frameId = int(data.get('frameId'))
	insertTime = int(data.get('insertTime'))
	insertedBy = int(data.get('insertedBy'))
	isCrash = int(data.get('isCrash'))
	isSevere = int(data.get('isSevere'))
	latitude = float(data.get('latitude'))
	longitude = float(data.get('longitude'))
	modelVersion = data.get('modelVersion')
	appVersion = data.get('appVersion')
	frameUTCLocalTime = data.get('frameUTCLocalTime')
	insertDate = int(data.get('insertDate'))
	insertLocalTime = data.get('insertLocalTime')
	modifiedTime = int(data.get('modifiedTime'))
	userId = data.get('userId')

	trip_metadata = str(tripId) + "," + `frameUTCTime` + "," + str(codeVersion) + "," + `crashProb` + "," + `crashSeverity` + "," + `frameDuration` + "," + `frameId` + "," + `insertTime` + "," + `insertedBy` + "," + `isCrash` + "," + `isSevere` + "," + `latitude` + "," + `longitude` + "," + str(modelVersion) + "," + str(appVersion) + "," + str(deviceModel_ouput_str) + "," + str(frameUTCLocalTime) + "," + `insertDate` + "," + str(insertLocalTime) + "," + `modifiedTime` + "," + str(os) + "," + str(userId)
	return {'Data' : trip_metadata + "\n"}

    def upload_data(self, path):
	"""
	This method is used to upload data into S3.
        :param path: Provide path has to be load the data.
        """
	bucket_key = "agero/prod/datapipeline/dynamodb-backup/prod-acd/acd-event-metadata-" + str(self.time.get_date(1)) + ".csv.gz"
	self.s3.upload_object(path, self.bucket, bucket_key)

    def put_record(self, records):
	"""
	This method is used to load records to Firehose Stream.
        :param records: Provide records has to be load.
        """
	self.firehose.put_records(records)
